Tradução do artigo publicado no jornal "Avanços em Fisíca".



Autores:Namiko Mitarai e Franco Nori.



Departament of physics,kyushu University 33,Fukuoka 812-8581,japão.



Frontier Research System, Instituto de Pesquisa Fisíca e Química.



(RIKEN),Hirosawa 2-1, wako-shi,saitam 351-0198, japão.



Fisíca Teórica,Departamento de Fisíca, Centro de Estudos dos estudos complexos

da universidade de Michigan,Ann Arbor,Michigan 48109-1040,EUA.



(Recebido em 26 de julho de 2005;na forma final em 2 de setembro de 2005).



Resumo:A maioria dos estudos sobre fisíca granular tem como foco a mídia granular seca sem liquídos entre os grãos. No entanto,em geologia e muitas aplicações do mundo real ( por exemplo,processamento de alimentos, produtos farmacêuticos, cerâmica, engenharia civil,construção,e muitas outras aplicações industriais ), líquido está presente entre os grãos.Esta produz coesão intergrãos e modifica drasticamente a mecânica propriedades da mídia granular( por exemplo, o ângulo de superfície pode ser maior do que 90 graus ). Aqui, apresentamos uma revisão das propriedades mecânicas do granulado úmido meios de comunicação, com particular ênfase no efeito de coesão. Também listamos vários problemas abertos que podem motivar estudos futuros neste tema,e principalmente campos inexplorados.

Palavras-chave: Granular material; Grãos úmidos; coesão. 





# Materiais granulares úmidos



## Introdução



### Física granular e mídia granular úmida



Os materiais granulares são coleções de partículas macroscópicas, como contas de vidro ou areia, que são visíveis a olho nu. Por causa do tamanho macroscópico das partículas, ruído térmico não desempenha nenhum papel no movimento das partículas, e as interações partícula-partícula são dissipativo. Portanto, a entrada de energia contínua por forças externas (gravidade, vibrações, etc.) são necessárias para mante-los em movimento. Partículas podem ficar em repouso como um sólido, flui como um líquido ou se comporta como um gás, dependendo da taxa de entrada de energia. No entanto, a força externa muitas vezes não é suficiente para as partículas para explorar seu espaço de fase, o que os torna bastante diferentes do convencional sistemas moleculares. O estudo científico de meios granulares tem uma longa história, principalmente no campo da engenharia, e muitos físicos se juntaram à comunidade de pesquisa granular nas últimas décadas. A maioria dos estudos em meios granulares, especialmente no campo da física, têm se concentrado em granular seco materiais, onde os efeitos dos fluidos intersticiais são insignificantes para a partícula dinâmica. Para meios granulares secos, as interações dominantes são colisões inelásticas e fricção, que são de curto alcance e não coesos. Mesmo nesta situação idealizada, a mídia granular seca mostra um comportamento único e marcante, que atraiu a atenção de muitos cientistas por séculos. No mundo real, no entanto, muitas vezes vemos materiais granulares úmidos, como praia areia. Materiais granulares secos e úmidos têm muitos aspectos em comum, mas há uma grande diferença: os materiais granulares úmidos são coesos devido à tensão superficial. No passado, muitos grupos de pesquisa que estudavam física granular foram lutando para minimizar a umidade e evitar forças coesivas Inter granulares. Na verdade, alguns experimentos foram realizados em câmaras de vácuo. Umidade e fluidos em geral, eram vistos como um incômodo a ser evitado a todo custo. No entanto, muitas importantes aplicações da vida real envolvem propriedades mecânicas de mídia granular úmida. Os exemplos incluem deslizamentos de terra induzidos pela chuva, produtos farmacêuticos, processamento de alimentos, mineração e indústrias de construção. Assim, é importante estudar a resposta mecânica de matéria granular com vários graus de umidade ou conteúdo líquido. Nesta revisão, mostramos como a coesão induzida pelo líquido muda as propriedades mecânicas dos materiais granulares. Consideramos principalmente estático ou situações quase estáticas, onde a coesão domina outros efeitos do líquido, como lubrificação e viscosidade. Alguns fenômenos neste campo que são não bem compreendidos são apresentados abaixo como "problemas em aberto". A maioria das referências listadas no final foco em resultados experimentais. Abordagens teóricas podem ser encontradas em os comentários citados abaixo e as referências neles. 



### O que é diferente na mídia granular seca?



 Estudos de mídia granular úmida foram feitos em muitas aplicações industriais. o propriedades mecânicas de meios granulares úmidos também são extremamente importantes em geologia e engenharia civil. Por exemplo, vamos considerar uma grande e cara obra civil projeto, a construção do Aeroporto Internacional de Kansai, em uma ilha artificial perto de Osaka. O peso dos 180 milhões de metros cúbicos de aterro e instalações comprimiu o fundo do mar, composto de argila, inevitavelmente fazendo com que o aeroporto afundasse alguma quantia. O aeroporto afundou 11,7 metros em média no final de 2000, e o assentamento ainda é monitorado cuidadosamente. Isso com outros exemplos de geologia e engenharia civil enfatizam a necessidade de compreender melhor a mecânica propriedades de conjuntos granulares úmidos, pequenos e grandes. 



O maior efeito que o líquido em meio granular induz é a coesão entre os grãos. Mesmo a umidade do ar pode resultar em uma pequena ponte de líquido em um ponto de contato, que introduz coesão. A coesão ocorre em granulado úmido material, a menos que o sistema se torne excessivamente úmido, ou seja, o meio granular está completamente imerso em um líquido. Nesta breve revisão, enfocamos o efeito dessa coesão; o sistema que estamos considerando é um material granular parcialmente úmido, que é uma mistura de grãos sólidos, líquidos e ar.



Além da coesão, existem muitos efeitos induzidos pela presença de o liquido. Um deles é a lubrificação do atrito sólido-sólido. Além do mais,a viscosidade do líquido pode induzir comportamento dependente da velocidade e dissipação adicional.



Esses efeitos são frequentemente vistos em experimentos subaquáticos. A escala de tempo do movimento do líquido (como o líquido se move ou flui através da mídia granular ) também afeta a dinâmica. Todos esses efeitos, é claro, são importantes.



 <img src = "img/c.jpeg" width="400">



Figura 1-(cor online). (a) Pilha de areia seca com um ângulo de superfície bem definido. (b) Pilha de areia úmida com um túnel papéis nas propriedades da mídia granular úmida. No entanto, estes são mais ou menos fenômenos dependentes da velocidade; no regime estático ou quase estático, a coesão muitas vezes desempenha o papel mais importante, fornecendo uma diferença qualitativa significativa a partir de meios granulares secos.



A situação mais simples onde vemos o efeito de coesão em meios granulares úmidos seriam as pilhas de areia que as crianças fazem em uma caixa de areia. Vamos primeiro considerar uma areia feita de grãos secos e, em seguida, grãos úmidos.



Quando fazemos uma pilha de areia usando areia seca (figura 1) (aa superfície da pilha é suave, formando um ângulo finito e bem definido, que é cerca de 35. Uma pilha ligeiramente mais densa pode ser feito batendo na superfície, mas não mudaria a forma da pilha. Mesmo se tentarmos fazer uma pilha mais nítida, despejando mais areia seca, os grãos fluem para baixo ao longo da superfície da pilha, e o ângulo resultante é sempre em torno do mesmo valor.



É fácil retirar parte da areia da pilha, mas não podemos fazer um túnel através de uma pilha de areia seca porque a parede em torno do buraco desabaria, e o ângulo da superfície não pode ser maior do que o ângulo crítico.



Vamos adicionar um pouco de água à areia e fazer outra pilha de areia (figura 1 (b)). Nós podemos tentar encontrar a quantidade ideal de água para produzir uma determinada forma. Pequenas quantidades de água não permitem a criação de muitas formas, embora muita água resulta em água turva que não consegue manter uma forma. Com a quantidade adequada de água, podemos fazer uma pilha de areia com o ângulo da superfície maior do que a caixa seca.



Na verdade, o ângulo pode ser tão grande quanto 90, ou até maior, permitindo a construção de elaborados e impressionantes castelos de areia (ver, por exemplo, a figura 2). Esta pilha molhada pode ser feita mais denso e forte batendo na superfície. Agora podemos fazer um túnel através da pilha, se formos suficientemente cuidadosos. Se tentarmos fazer um buraco muito grande, a pilha quebraria, formando algumas superfícies ásperas portanto, em uma caixa de areia, já aprendemos propriedades importantes do granulado seco mídia: um ângulo finito de repouso, pequena histerese na embalagem e pequena resistência contra Carregando. Também aprendemos como essas propriedades são drasticamente alteradas ou aprimoradas por adicionar um líquido, resultando em: um ângulo muito maior de repouso, histerese mais forte em embalagem e maior resistência ao carregamento. Essas comparações são resumidas na tabela 1. Nas seções a seguir, veremos como esse comportamento é estudado por cientistas.



 <img src = "img/d.jpeg" width="300">



Figura-Castelo de areia feito de areia molhada (Copyright S Landscapes 2005).



## Meio granular úmido: grãos com líquido e ar.



### Coesão entre duas esferas.



#### Menisco e sucção.



A coesão na mídia granular úmida surge da superfície tensão e efeitos capilares do líquido. Considere um menisco entre ar com pressão $`Pa`$ e líquido com pressão $`P1`$. A diferença de pressão $`P`$ entre o líquido e ar com um


 $`\Delta P = P_a - P_1 = \gamma \left(\frac{1}{r_1} +\frac{1}{r_2}\right)`$ 



Tabela 1. Comparação das propriedades físicas entre a matéria granular seca e úmida.



 <img src = "img/f.jpeg" width="600">



O comprimento capilar



$`a = \sqrt{\frac{2\gamma}{\rho_1 g}}`$



O comprimento capilar dá a escala de comprimento que compara a força capilar e a gravidade, onde $`g`$ é o aceleração gravitacional $`el`$ é a densidade de massa do líquido; $`a`$ tem cerca de 3,9 mm para água à temperatura ambiente. A força capilar torna-se dominante quando o escalas de comprimento relevantes são muito menores do que $`a`$. A seguir, consideramos a situação onde a força capilar é dominante. Em geral, é preciso considerar o comprimento capilar ao interpretar os resultados experimentais, porque a maioria dos experimentos são conduzido sob gravidade.



### Ponte líquida entre duas esferas.

Agora vamos ver como o líquido induz coesão em meios granulares, considerando uma ponte líquida entre duas esferas como mostrado na figura 3. A força de atração entre as esferas devido ao líquido meniscos é dado pela soma da tensão superficial e ponte líquida entre duas esferas. Agora vamos ver como o líquido induz coesão em meios granulares, considerando uma ponte líquida entre duas esferas como mostrado na figura 3. A força de atração entre as esferas devido ao líquido menisco é dado pela soma da tensão superficial e da sucção; quando estimamos a força no pescoço da ponte, é dada por



$`F_{bridge} = 2 \pi r_2 \gamma+ \pi r_2^2 \Delta P`$



Com



$`\Delta P = \gamma \left(\frac{1}{r_1} -\frac{1}{r_2}\right)`$ 



Em meios granulares parcialmente úmidos reais, a imagem de uma ponte líquida formada entre partículas completamente esféricas muitas vezes não é suficiente para descrever a interação.



No entanto, a presença de um líquido, que tenta minimizar sua área superficial, geralmente resulta em sucção e uma força coesiva entre as partículas.



 <img src = "img/g.jpeg" width="300">



Figura 3- Diagrama esquemático de uma ponte líquida entre esferas idênticas.



### Meio granular úmido com vários conteúdos líquidos.



#### Quatro estados de conteúdo líquido: pendular, funicular, capilar e pasta Estado. 



É bem conhecido que a coesão em materiais granulares úmidos depende da quantidade de líquido no sistema. Os quatro regimes de conteúdo líquido distinguido em meio granular úmido:



- Estado pendular: as partículas são mantidas juntas por pontes de líquido em seu ponto de contato.



- Estado funicular: alguns poros estão totalmente saturados por líquido, mas ainda permanecem vazios cheios de ar.



- Estado capilar: Todos os vazios entre as partículas são preenchidos com líquido, mas o líquido da superfície é puxado de volta para os poros sob a ação capilar.



- Estado de pasta: as partículas estão totalmente imersas no líquido e na superfície do líquido é convexa, ou seja, nenhuma ação capilar na superfície.



- Esses quatro regimes são mostrados esquematicamente na tabela 2. 



A coesão surge nos estados pendular, funicular e capilar. No entanto, nós consideraremos esses três estados nesta revisão. A priori, as propriedades mecânicas de seria de se esperar que esses três estados fossem qualitativamente diferentes. No caso do estado pendular, a força coesiva entre um par de grãos atua através de um líquido ponte; enquanto no estado capilar, a interface entre o líquido e o ar é pressionado devido à sucção, e essa pressão mantém juntos todos os grãos no fase líquida. Tanto a coesão de dois corpos devido às pontes de líquido quanto a sucção nas interfaces líquido-ar desempenham papéis importantes no estado funicular.



É muito difícil observar diretamente a distribuição do líquido em três dimensões conjuntos granulares, embora a distribuição de líquido deva ser facilmente observável para agregados granulares úmidos bidimensionais confinados por placas de Plexiglas.



Como veremos mais tarde, pontes líquidas em três dimensões para conteúdo líquido bastante pequeno.



Tabela 2. Meios granulares com várias quantidades de líquido. Nos diagramas esquemáticos na terceira coluna, os círculos preenchidos representam os grãos e as regiões cinzas representam o líquido intersticial. Diagrama esquemático do estado do conteúdo líquido Descrição física.



 <img src="img/h.jpeg" width= "600" >



Foram recentemente visualizados usando técnicas de correspondência de índice , mas parece ser difícil estender o método para um conteúdo líquido muito maior.



A distribuição detalhada de líquido em cada regime de conteúdo de líquido descrito na tabela 2 ainda permanece como um dos muitos "problemas em aberto" que listamos abaixo como áreas que não foram suficientemente bem estudados até agora.



Convencionalmente, esses regimes de conteúdo líquido foram distinguidos pela medição a relação entre o conteúdo de líquido $`S`$ e a sucção $`\Delta P`$ como veremos abaixo, onde $`S`$ é a razão entre o volume de líquido $`V1`$ no sistema e o volume de os vazios $`Vv`$ na mídia granular (quando o volume total do sistema é $`Vt`$ e o volume ocupado pelos grãos é $`Vs`$, então $` Vv = Vt -Vs `$ e $` Vv = V1-Va`$ onde $` Va `$ é o volume de ar no sistema). Alguns autores definem $`S`$ como a porcentagem do líquido conteúdo (ou seja, multiplicando a razão de $`S = V1 =/Vv `$ por 100). Aqui, qualquer caso (proporção ou percentagem) é claro no texto.



#### Conteúdo líquido e sucção.



### Medição de sucção em meios granulares. 



Existem várias maneiras de medir a sucção $`P`$ em meio granular, e o método apropriado deve ser escolhido em relação à faixa de medição de $`P`$. A Tabela 3. mostra a lista de métodos e o intervalo de medições comumente usado na mecânica do solo (onde o Tabela 3. Lista de métodos para medir a sucção e faixa prática de sucção para cada medição. Métodos baseados na diferença de pressão osmótica (por exemplo, devido a gradientes de concentração de soluto) não estão listados abaixo.



  <img src= "img/u.jpeg" width = "500" >    



Método do papel de filtro de contato Toda a gama mistura de grãos do solo e água é considerada como uma amostra) . Está além do escopo desta breve revisão para descrever todos os métodos experimentais usados, mas teses são descritos nas referências. Como exemplos, a seguir, descrevemos dois usados com frequência métodos, chamados de técnicas de translação de eixo e tensiômetros, que fazem uso da pressão capilar em um disco cerâmico poroso para medir e controlar o sucção.



A sucção relativamente baixa pode ser medida ou controlada usando tensiômetros e a técnica de translação do eixo. Um diagrama esquemático para descrever esses métodos é dado na figura 4; esses métodos fazem uso das propriedades do que é chamado de 'Material de alta entrada de ar $`(HAE)`$, que é um material com muitos poros microscópicos,como uma cerâmica porosa. Na figura 4, um contêiner é separado em duas partes por um $`HAE`$ disco; a parte inferior da caixa é preenchida com um líquido com pressão $`P`$, enquanto a parte superior é preenchida com ar com pressão $`Pa`$. Uma visão ampliada do disco $`HAE`$ é dado na inserção (a), onde os poros do material $`HAE`$ estão saturados com líquido. Lá, a tensão superficial nas interfaces líquidas formadas nos poros permitem um diferença finita de pressão entre o líquido e o ar. Considerando a equação (2) e a vista ampliada (a) na figura 4, a maior diferença de pressão possível entre o ar e o líquido no material $`HAE`$ é aproximadamente dado por:



$`\Delta P_{max} = \frac{2\gamma}{R_{max}}`$



Aqui, como uma primeira aproximação, assumimos que os poros são em forma de tubo com raio máximo $`R_{max}`$; para tamanhos de poros menores,$`\Delta P_{max}`$ seria maior. O liquido pressão $`P1`$ na parte inferior do recipiente e a pressão do ar $`Pa`$ na parte superior parte pode ser controlada separadamente sem permitir que o ar entre na parte inferior do o recipiente, desde que $`(Pa- Pl)<\Delta P_{max}`$ seja satisfeito.



Agora, vamos colocar um material granular úmido no disco $`HAE`$ dentro do contêiner (figura 4, inserção (b)). Quando o equilíbrio é alcançado após uma longa espera 1 Quando os solutos são dissolvidos na água, no solo, os efeitos osmóticos produzem o produto químico diferença potencial da água livre, onde água livre é a água que não contém soluto, não tem interação com outras fases que produzem curvatura para a interface ar-água, e não tem nenhuma força externa além da gravidade . Na mecânica do solo, esse potencial químico diferença às vezes é medida na unidade de pressão e é chamada de osmótica sucção. A diferença de pressão entre o ar e o líquido (água) no solo $` \Delta P=Pa-P1`$, que é devido ao efeito capilar e adsorção de curto prazo de água às superfícies dos grãos, é referido para sucção de matric. Neste artigo, nos concentramos no último, e $`P`$ simplesmente referido como sucção.



 <img src = "img/j.jpeg" width="400">



Figura 4..Diagrama esquemático que descreve os tensiômetros e a translação dos eixos técnicos. Os círculos preenchidos representam grãos e a região em azul claro representa um líquido. O recipiente é separado por um disco poroso de alta entrada de ar $`(HAE)`$ que possui muitos microscópicos poros. A parte superior é preenchida com ar com pressão $`Pa`$, e a parte inferior é preenchida com líquido com pressão $`P1`$. Os poros do disco poroso são preenchidos com líquido, e os meniscos no topo superfície dos poros no disco poroso (uma vista ampliada é mostrada na inserção (a)) torna possível para manter $`P1<Pa`$. Um conjunto granular úmido é colocado no disco poroso (uma visão ampliada é mostrado na inserção (b)), e o líquido na mídia granular úmida está em bom contato com o líquido no disco poroso (uma vista ampliada é mostrada na inserção (c)); a pressão do líquido em torno do molhado grãos é igual à pressão $`P1`$ do líquido a granel no fundo do recipiente. Adaptado de tempo, a pressão do líquido no meio granular úmido deve ser igual ao líquido pressão $`P1`$ na parte inferior do recipiente, se a fase líquida no granular meio está em um bom contato com o líquido nos poros do disco $`HAE`$ (figura 4, inserção (c)). A pressão do ar $`Pa`$ e a pressão do líquido $`P1`$ nas partes superior e inferior do recipiente respectivamente, podem ser facilmente medidos, ou mesmo controlados, e a sucção $`\Delta P`$ no material granular úmido é dada por $`\Delta P=Pa-P1`$.
O método para controlar a sucção $`\Delta P`$ controlando as pressões de líquido e ar separadamente é chamada de técnica de translação do eixo .

Inicialmente um granular saturado o material é colocado em um disco $`HAE`$ que separa o líquido e o ar, como esquematicamente mostrado na figura 4, e o líquido no conjunto granular é drenado até o sistema atingir o equilíbrio, onde a pressão do líquido no meio granular torna-se igual ao do líquido a granel na caixa inferior. A ferramenta para medir o a sucção usando o disco $`HAE`$ é chamada de tensiômetros, que é composto por um copo feito de uma cerâmica $`HAE`$, e um sensor para medir a pressão do líquido, conectado por um tubo cheio de líquido (água). O copo de cerâmica $`HAE`$ é colocado no conjunto granular molhado, e a pressão dos poros é medida por uma troca direta de líquido entre o sensor e o meio granular úmido. Observe que, ao usar qualquer a técnica de translação do eixo ou um tensiômetros, o valor máximo possível do a sucção é limitada por $`P_max`$; outros métodos usados para medir diferenças de pressão maiores estão listados na tabela 3.



Figura 5. Relação entre o conteúdo líquido e sucção. A descrição esquemática da sucção $`P`$ versus o conteúdo de líquido $`S`$ é mostrado na figura 5. Duas linhas são mostrados porque há uma histerese nos processos de umedecimento e secagem. Em ambos 



 <img src = "img/y.jpeg" width="400">
 
Figura 5.Um gráfico esquemático da sucção $`\Delta P`$ versus o conteúdo de líquido $`S`$. A sucção é mostra da em uma escala logarítmica. A curva superior representa o processo de secagem e a curva inferior representa o processo de umedecimento. Ele muda a inclinação significativamente perto dos limites de fase entre os estados pendular, funicular e capilar. A inclinação para baixo e grande conteúdo líquido é muito maior do que para intermediários conteúdo líquido. Esta mudança de inclinação reflete a distribuição de líquido em mídia granular. Para simplificar, vamos considerar o processo de secagem (a drenagem do líquido) de meios granulares completamente saturados. No início, o material está no estado de pasta e a pressão do líquido é igual ou maior que a pressão do ar. Depois de um apropriado quantidade de líquido é drenada, a mídia está no estado capilar e a pressão do líquido torna-se mais baixa do que a pressão do ar, ou seja, a sucção $`P`$ torna-se positiva. Quando nós diminuir ainda mais o conteúdo de líquido $`S`$ no estado capilar, $`\Delta P`$ aumenta muito rapidamente ao diminuir $` S;\Delta P`$ é determinado pela curvatura da superfície do líquido, e a mudança da curvatura da superfície requer pouca mudança de conteúdo de líquido $`S`$ porque o líquido a granel permanece no sistema. Quando $`\Delta P`$ excede um valor crítico, que é principalmente determinada pela tensão superficial e a geometria da mídia granular em torno do líquido da superfície, o ar começa a entrar na maior parte do sistema estado funicular é obtido. A sucção ainda aumenta à medida que o conteúdo líquido diminui, mas sua taxa de aumento torna-se muito menor do que no estado capilar, porque o líquido nos poros na massa precisa ser removido. Em algum ponto, a maioria dos vazios serão preenchidos com ar, e o resto do líquido é encontrado ao redor do pontos de contato entre os grãos vizinhos e na fina película líquida que cobre as superfícies dos grãos. Ou seja, o sistema está no estado pendular. Neste pendular estado, uma pequena mudança no conteúdo líquido novamente resulta em uma mudança relativamente grande de a sucção, porque a quantidade total de líquido é muito menor que o vazio volume $`Vv`$, e a mudança de curvatura em cada ponte líquida requer apenas uma pequena mudança do conteúdo líquido $`S`$. Conforme mostrado na figura 5, para um determinado valor de sucção $`\Delta P `$ , o conteúdo de líquido $`S`$ é menor para o processo de umedecimento de meios granulares inicialmente secos do que para a secagem processo de grãos inicialmente saturados. Isso se deve em parte à histerese em os processos de umedecimento e secagem em superfícies sólidas , e em parte porque a variação do tamanho dos poros no meio granular.